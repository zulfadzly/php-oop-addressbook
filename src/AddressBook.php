<?php namespace MohdZulfadzly\AddressBook;

use Kint;

/**
 * Phyiscal address
 */
class AddressBook {
    // Street address
    public $street_address_1;
    public $street_address_2;
    
    // City name
    public $city_name;
    
    // Subdivison name
    public $subdivision_name;
    
    // Postcode
    public $postal_code;
    
    // Country name
    public $country_name;
}
